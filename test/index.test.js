const chai = require('chai');
const chaiHttp = require('chai-http');
const app = require('../src/index');

chai.use(chaiHttp);
chai.should();

describe("App", () => {
  describe("GET /hello with parameter", () => {
    it("should return greeting with given name", (done) => {
      chai.request(app)
      .get('/hello')
      .query({name: "JANI"})
      .end((err, res) => {
        res.should.have.status(200);
        res.text.should.include('Hello JANI!');
        done();
      });
    });
    // Test to get single student record
    it("should return greeting with default value", (done) => {
      chai.request(app)
      .get('/hello')
      .end((err, res) => {
        res.should.have.status(200);
        res.text.should.include('Hello World!');
        done();
      });
    });
  });
});