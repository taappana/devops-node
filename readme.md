# Devops Akatemia Node-sovellus

## Ajoympäristö
```
Testattu
Node v8.10.0
npm v6.9.0
```

## Lokaali ajo
```
// Suorita samasta kansiosta, missä package.json sijaitsee
npm start

// Mene selaimella osoitteeseen localhost:3000/hello?name=Testaaja
// Sivulla pitäisi näkyä "Hello Testaaja!"
// Ilman parametreja pitäisi näkyä "Hello World!
```

## Aja yksikkötestit
```
// Suorita samasta kansiosta, missä package.json sijaitsee
npm test

```