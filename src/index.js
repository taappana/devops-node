const express = require('express');
const app = express();
const port = 8080;

app.set('view engine', 'ejs');

app.get('/hello', (req, res) => {
  let name = "World";
  if (req.query && req.query.name) {
    name = req.query.name;
  }
  res.status(200).render('hello', {name})
});

app.listen(port, () => console.log(`App listening on port ${port}!`));

module.exports = app;